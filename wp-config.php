<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'usesalx');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'tiger');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'rzk{9wQ~_?bMVWh(u/%CB;v_V%RNZUX.=t`J5suxb6<e&qLSl,qu*5R9(quZ1lGB');
define('SECURE_AUTH_KEY',  'n[+&*,=dgwEBzG:9HrX_Mr1Vz>EN;P-S|{JmQFDU)-bFl3>[l <(rYyHb-P6Ro|.');
define('LOGGED_IN_KEY',    '.wza. EaVS{S(zOy5N*Df6@1)*;pE;]gS=Btgyiiinoi`18Jm<H5qU;uzV5b`g*o');
define('NONCE_KEY',        'azVME<zwsM8apln3l4tp|$A;I#OI..xWGE465815MukZwxzmEX52#+YHfE}OC~V2');
define('AUTH_SALT',        ']}OV`bh_,|Aw-d4w4&8Il20e )N{cin?P)<#FI[GE7D+s(U- r{Z-o-^$w/Z.P{4');
define('SECURE_AUTH_SALT', '0Ba.mt5}i)yEF=b4dY{0AKA7O-X?8M_5n6{&-HF}!/@w4l0je6phDuJDn&TL:QwC');
define('LOGGED_IN_SALT',   'S;k1@Cr$mgvXui&e}ZGfB0vM6ghjL{+e}=eIg]x.4?E2nt}g}~LI>cpxN8GPb.M;');
define('NONCE_SALT',       ',Sb a?~fYjOVCBr1{1q`0.vM,sqTE8D8&V{K1B08O?I*)kKzNjl (6<.21HoVRhw');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

