<?php
/**
 * Template Name: signup
 *
 * @package WordPress
 * @subpackage usesalx
 * @since version 1.0
 * @author AnanthJP
 * @Date : 26/04/2017
 */
?>
<?php
//Redirect to home page when user already login
//Ref link : https://haveposts.com/blog/2013/01/25/watch-movie-online-silence-2016-subtitle-english/
if (is_user_logged_in()) {
    wp_redirect(home_url());
    exit();
}
?>
<?php get_header(); ?>
<!-- Page Header Start -->
<div class="page-header" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/banner1.jpg);">
    <div class="container">
        <div class="row">         
            <div class="col-md-12">
                <div class="breadcrumb-wrapper">
                    <h2 class="page-title">join to us</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page Header End --> 
<?php
$errors = array();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    // Check username is present and not already in use
    $username = $wpdb->escape($_REQUEST['username']);
    if (strpos($username, ' ') !== false) {
        $errors['username'] = "Sorry, no spaces allowed in usernames";
    }
    if (empty($username)) {
        $errors['username'] = "Please enter a username";
    } elseif (username_exists($username)) {
        $errors['username'] = "Username already exists, please try another";
    }

    // Check email address is present and valid
    $email = $wpdb->escape($_REQUEST['email']);
    if (!is_email($email)) {
        $errors['email'] = "Please enter a valid email";
    } elseif (email_exists($email)) {
        $errors['email'] = "This email address is already in use";
    }

    // Check password is valid
    if (0 === preg_match("/.{6,}/", $_POST['password'])) {
        $errors['password'] = "Password must be at least six characters";
    }

    // Check password confirmation_matches
    if (0 !== strcmp($_POST['password'], $_POST['password_confirmation'])) {
        $errors['password_confirmation'] = "Passwords do not match";
    }

    // Check terms of service is agreed to    
    if ($_POST['terms'] != "yes") {
        echo $errors['terms'] = "You must agree to Terms of Service";
    }

    if (0 === count($errors)) {
        $password = $_POST['password'];
        $new_user_id = wp_create_user($username, $password, $email);
        // You could do all manner of other things here like send an email to the user, etc. I leave that to you.
        $success = 1;
        header('Location:' . get_bloginfo('url') . '/login/?success=1&u=' . $username);
    }
}
?>
<!-- Content section Start --> 
<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                <div class="page-login-form box">
                    <h3>
                        Register
                    </h3>                    
                    <form role="form" class="login-form" method="post">
                        <div class="form-group">
                            <div class="input-icon">
                                <i class="icon fa fa-user"></i>
                                <input type="text" id="sender-email" class="form-control" name="username" placeholder="Username" value="<?php echo $userName = isset($_POST['username'])&&($_POST['username'] !="")?$_POST['username']:"";?>">
                            </div>
                            <p class="custom_login_err"><?php echo $error= isset($errors['username'])&&$errors['username'] !=""?$errors['username']:""?></p>
                        </div> 
                        <div class="form-group">
                            <div class="input-icon">
                                <i class="icon fa fa-envelope"></i>
                                <input type="text" id="sender-email" class="form-control" name="email" placeholder="Email Address" value="<?php echo $email = isset($_POST['email'])&&($_POST['email'] !="")?$_POST['email']:"";?>">
                            </div>
                            <p class="custom_login_err"><?php echo $error= isset($errors['email'])&&$errors['email'] !=""?$errors['email']:""?></p>
                        </div> 
                        <div class="form-group">
                            <div class="input-icon">
                                <i class="icon fa fa-unlock-alt"></i>
                                <input type="password" name="password" class="form-control" placeholder="Password" value="<?php echo $pass = isset($_POST['password'])&&($_POST['password'] !="")?$_POST['password']:"";?>">
                            </div>
                            <p class="custom_login_err"><?php echo $error= isset($errors['password'])&&$errors['password'] !=""?$errors['password']:""?></p>
                        </div>  
                        <div class="form-group">
                            <div class="input-icon">
                                <i class="icon fa fa-unlock-alt"></i>
                                <input type="password" name="password_confirmation" class="form-control" placeholder="Retype Password" value="<?php echo $cpass = isset($_POST['password_confirmation'])&&($_POST['password_confirmation'] !="")?$_POST['password_confirmation']:"";?>">
                            </div>
                            <p class="custom_login_err"><?php echo $error= isset($errors['password_confirmation'])&&$errors['password_confirmation'] !=""?$errors['password_confirmation']:""?></p>
                        </div>                 
                        <div class="checkbox">
                            <input type="checkbox" id="remember" name="terms" value="yes" style="float: left;">
                            <label for="remember">By creating account you agree to our Terms & Conditions</label>
                            <p class="custom_login_err"><?php echo $error= isset($errors['terms'])&&$errors['terms'] !=""?$errors['terms']:""?></p>
                        </div>                        
                        <button type="submit" class="btn btn-common log-btn">Register</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Content section End -->
<?php get_footer(); ?>

