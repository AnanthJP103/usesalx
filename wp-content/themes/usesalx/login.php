<?php
/**
 * Template Name: login
 *
 * @package WordPress
 * @subpackage usesalx
 * @since version 1.0
 * @author AnanthJP
 * @Date : 26/04/2017
 */
?>
<?php
//Redirect to home page when user already login
if (is_user_logged_in()) {
    wp_redirect(home_url());
    exit();
}
?>
<?php get_header(); ?>
<!-- Page Header Start -->
<div class="page-header" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/banner1.jpg);">
    <div class="container">
        <div class="row">         
            <div class="col-md-12">
                <div class="breadcrumb-wrapper">
                    <h2 class="page-title"><?php echo get_the_title(); ?></h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Page Header End -->
<?php
//Login form validation and success login to redirect on home page
global $wpdb;
$err = '';
$success = '';

if (isset($_POST['login']) && $_POST['login'] == 'login') {
    //We shall SQL escape all inputs to avoid sql injection.
    $username = $wpdb->escape($_POST['email']);
    $password = $wpdb->escape($_POST['pwd']);
    $remember = $wpdb->escape($_POST['rememberme']);

    if ($username == "" || $password == "") {
        $err = 'Please don\'t leave the required field.';
    } else {
        $user_data = array();
        $user_data['user_login'] = $username;
        $user_data['user_password'] = $password;
        $user_data['remember'] = $remember;
        $user = wp_signon($user_data, false);

        if (is_wp_error($user)) {
            $err = $user->get_error_message();
        } else {
            wp_set_current_user($user->ID, $username);
            do_action('set_current_user');
            wp_redirect(home_url());
            exit();
        }
    }
}
?>
<!-- Content section Start --> 
<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-4 col-md-4 col-md-offset-4">
                <div class="page-login-form box">
                    <h3>
                        Login
                    </h3>
                    <p class="custom_login_err">
                        <?php
                            if (!empty($err)) {
                                echo $err;
                            }
                        ?>
                    </p>
                    <p class="register_success">
                        <?php
                            echo $reg = isset($_GET['success']) && ($_GET['success'] == 1) ? "Registration Successful, please login to your account.": ""; 
                        ?>                        
                    </p>
                    <form role="form" class="login-form" method="post">
                        <div class="form-group">
                            <div class="input-icon">
                                <i class="icon fa fa-user"></i>
                                <input type="text" id="sender-email" class="form-control" name="email" placeholder="email">
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="input-icon">
                                <i class="icon fa fa-unlock-alt"></i>
                                <input type="password" class="form-control" placeholder="Password" name="pwd">
                            </div>
                        </div>                  
                        <div class="checkbox">
                            <input type="checkbox" id="remember" name="rememberme" value="forever" style="float: left;">
                            <label for="remember">Remember me</label>
                        </div>
                        <input type="hidden" name="login" value="login" />
                        <button type="submit" class="btn btn-common log-btn">Submit</button>
                    </form>
                    <ul class="form-links">
                        <li class="pull-left"><a href="<?php echo get_permalink(get_page_by_title('join')); ?>">Don't have an account?</a></li>
                        <li class="pull-right"><a href="forgot-password.html">Lost your password?</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Content section End -->
<?php get_footer(); ?>