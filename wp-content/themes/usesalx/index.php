<?php
/**
 * Template Name: home
 *
 * @package WordPress
 * @subpackage usesalx
 * @since version 1.0
 * @author AnanthJP
 * @Date : 25/04/2017
 * @Modified_Date : 25/04/2017
 * @Page_Description
 */
?>
<?php get_header(); ?>
<!-- Start intro section -->
<section id="intro" class="section-intro">
    <div class="overlay">
        <div class="container">
            <div class="main-text">
                <?php 
                    $home_page_details = get_page_by_title('home');                    
                    $home_id = $home_page_details->ID;
                    $home_welcome = !empty($home_page_details->post_content)?$home_page_details->post_content:"heading no found";
                ?>
                <h1 class="intro-title"><?php echo $home_welcome;?></h1>
                <p class="sub-title">
                    <?php
                        if(get_field('sub_title_home',$home_id)) {
                            the_field('sub_title_home',$home_id);
                        } else {
                            echo "NO Data Found";
                        }
                    ?>                    
                </p>
                <!-- Start Search box -->
                <div class="row search-bar">
                    <div class="advanced-search">
                        <form class="search-form" method="get">
                            <div class="col-md-3 col-sm-6 search-col">
                                <div class="input-group-addon search-category-container">
                                    <label class="styled-select">
                                        <select class="dropdown-product selectpicker" name="product-cat" >
                                            <option value="0">All Categories</option>
                                            <option class="subitem" value="community"> Community</option>
                                            <option value="items-for-sale"> Items For Sale</option>
                                            <option value="jobs"> Jobs</option>
                                            <option value="personals"> Personals</option>
                                            <option value="training"> Training</option>
                                            <option value="real_estate"> Real Estate</option>
                                            <option value="services"> Services</option>
                                            <option value="vehicles"> Vehicles</option>
                                        </select>                                    
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 search-col">
                                <div class="input-group-addon search-category-container">
                                    <label class="styled-select location-select">
                                        <select class="dropdown-product selectpicker" name="product-cat" >
                                            <option value="0">All Locations</option>
                                            <option value="New York">New York</option>
                                            <option value="California">California</option>
                                            <option value="Washington">Washington</option>
                                            <option value="churches">Birmingham</option>
                                            <option value="Birmingham">Chicago</option>
                                            <option value="Phoenix">Phoenix</option>
                                        </select>                                    
                                    </label>
                                </div>


                            </div>
                            <div class="col-md-3 col-sm-6 search-col">
                                <input class="form-control keyword" name="keyword" value="" placeholder="Enter Keyword" type="text">
                                <i class="fa fa-search"></i>
                            </div>
                            <div class="col-md-3 col-sm-6 search-col">
                                <button class="btn btn-common btn-search btn-block"><strong>Search</strong></button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- End Search box -->
            </div>
        </div>
    </div>
</section>
<!-- end intro section -->
<div id="CHART" style="width: 100%; background-color: #EEEEEE;height: 500px;"></div>
<div class="wrapper">
    <!-- Categories Homepage Section Start -->
    <section id="categories-homepage">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="section-title">Browse Ads from 8 Categories</h3>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="category-box border-1 wow fadeInUpQuick animated" data-wow-delay="0.3s" style="visibility: visible;-webkit-animation-delay: 0.3s; -moz-animation-delay: 0.3s; animation-delay: 0.3s;">
                        <div class="icon">
                            <a href="category.html"><i class="lnr lnr-users color-1"></i></a>
                        </div>
                        <div class="category-header">
                            <a href="category.html"><h4>Community</h4></a>
                        </div>
                        <div class="category-content">
                            <ul>
                                <li>
                                    <a href="category.html">Announcements</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Car Pool - Bike Ride</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Charity - Donate - NGO</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Lost - Found</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Tender Notices</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">General Entertainment</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">View all subcategories →</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="category-box border-2 wow fadeInUpQuick animated" data-wow-delay="0.6s" style="visibility: visible;-webkit-animation-delay: 0.6s; -moz-animation-delay: 0.6s; animation-delay: 0.6s;">
                        <div class="icon">
                            <a href="category.html"><i class="lnr lnr-laptop-phone color-2"></i></a>
                        </div>
                        <div class="category-header">
                            <a href="category.html"><h4>Electronics</h4></a>
                        </div>
                        <div class="category-content">
                            <ul>
                                <li>
                                    <a href="category.html">Home Electronics</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">LCDs</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Charity - Donate - NGO</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Mobile &amp; Tablets</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">TV &amp; DVDs</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Technical Services</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Others</a>
                                <sapn class="category-counter">1</sapn>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="category-box border-3 wow fadeInUpQuick animated" data-wow-delay="0.9s" style="visibility: visible;-webkit-animation-delay: 0.9s; -moz-animation-delay: 0.9s; animation-delay: 0.9s;">
                        <div class="icon">
                            <a href="category.html"><i class="lnr lnr-cog color-3"></i></a>
                        </div>
                        <div class="category-header">
                            <a href="category.html"><h4>Services</h4></a>
                        </div>
                        <div class="category-content">
                            <ul>
                                <li>
                                    <a href="category.html">Cleaning Services</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Educational</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Food Services</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Medical</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Office &amp; Home Removals</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">General Entertainment</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">View all subcategories →</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="category-box border-4 wow fadeInUpQuick animated" data-wow-delay="1.2s" style="visibility: visible;-webkit-animation-delay: 1.2s; -moz-animation-delay: 1.2s; animation-delay: 1.2s;">
                        <div class="icon">
                            <a href="category.html"><i class="lnr lnr-cart color-4"></i></a>
                        </div>
                        <div class="category-header">
                            <a href="category.html"><h4>Shopping</h4></a>
                        </div>
                        <div class="category-content">
                            <ul>
                                <li>
                                    <a href="category.html">Bags</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Beauty Products</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Jewelry</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Shoes M/F</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Tender Notices</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Others</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="category-box border-5 wow fadeInUpQuick animated" data-wow-delay="1.5s" style="visibility: visible;-webkit-animation-delay: 1.5s; -moz-animation-delay: 1.5s; animation-delay: 1.5s;">
                        <div class="icon">
                            <a href="category.html"><i class="lnr lnr-briefcase color-5"></i></a>
                        </div>
                        <div class="category-header">
                            <a href="category.html"><h4>Jobs</h4></a>
                        </div>
                        <div class="category-content">
                            <ul>
                                <li>
                                    <a href="category.html">Accounts Jobs</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Cleaning &amp; Washing</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Web design</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Design &amp; Code</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Finance Jobs</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Data Entry</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">View all subcategories →</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="category-box border-6 wow fadeInUpQuick animated" data-wow-delay="1.8s" style="visibility: visible;-webkit-animation-delay: 1.8s; -moz-animation-delay: 1.8s; animation-delay: 1.8s;">
                        <div class="icon">
                            <a href="category.html"><i class="lnr lnr-graduation-hat color-6"></i></a>
                        </div>
                        <div class="category-header">
                            <a href="category.html"><h4>Training</h4></a>
                        </div>
                        <div class="category-content">
                            <ul>
                                <li>
                                    <a href="category.html">Android Development</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">20 Days HTML/CSS</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">iOS Development with Swift</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">SEO for rest of us</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Mastering in Java</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Others</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">View all subcategories →</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="category-box border-7 wow fadeInUpQuick animated" data-wow-delay="2.1s" style="visibility: visible;-webkit-animation-delay: 2.1s; -moz-animation-delay: 2.1s; animation-delay: 2.1s;">
                        <div class="icon">
                            <a href="category.html"><i class="lnr lnr-apartment color-7"></i></a>
                        </div>
                        <div class="category-header">
                            <a href="category.html"><h4>Real Estate</h4></a>
                        </div>
                        <div class="category-content">
                            <ul>
                                <li>
                                    <a href="category.html">Farms</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Home for rent</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Hotels</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Land for sale</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Offices for rent</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Others</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="category-box border-8 wow fadeInUpQuick animated" data-wow-delay="2.3s" style="visibility: visible;-webkit-animation-delay: 2.3s; -moz-animation-delay: 2.3s; animation-delay: 2.3s;">
                        <div class="icon">
                            <a href="category.html"><i class="lnr lnr-car color-8"></i></a>
                        </div>
                        <div class="category-header">
                            <a href="category.html"><h4>Vehicles</h4></a>
                        </div>
                        <div class="category-content">
                            <ul>
                                <li>
                                    <a href="category.html">Cars</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Fancy Cars</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Kids Bikes</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Motor Bikes</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Classic &amp; Modern</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">Kinds</a>
                                <sapn class="category-counter">3</sapn>
                                </li>
                                <li>
                                    <a href="category.html">View all subcategories →</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Categories Homepage Section End -->

    <!-- Featured Listings Start -->
    <section class="featured-lis" >
        <div class="container">
            <div class="row">
                <div class="col-md-12 wow fadeIn" data-wow-delay="0.5s">
                    <h3 class="section-title">Featured Listings</h3>
                    <div id="new-products" class="owl-carousel">
                        <div class="item">
                            <div class="product-item">
                                <div class="carousel-thumb">
                                    <img src="<?php echo get_template_directory_uri() . '/assets/img/product/img8.jpg';?>" alt=""> 
                                    <div class="overlay">
                                        <a href="ads-details.html"><i class="fa fa-link"></i></a>
                                    </div> 
                                </div>    
                                <a href="ads-details.html" class="item-name">Lorem ipsum dolor sit</a>  
                                <span class="price">$150</span>  
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-item">
                                <div class="carousel-thumb">
                                    <img src="<?php echo get_template_directory_uri() . '/assets/img/product/img8.jpg';?>" alt=""> 
                                    <div class="overlay">
                                        <a href="ads-details.html"><i class="fa fa-link"></i></a>
                                    </div> 
                                </div> 
                                <a href="ads-details.html" class="item-name">Sed diam nonummy</a>  
                                <span class="price">$67</span> 
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-item">
                                <div class="carousel-thumb">
                                    <img src="<?php echo get_template_directory_uri() . '/assets/img/product/img8.jpg';?>" alt=""> 
                                    <div class="overlay">
                                        <a href="ads-details.html"><i class="fa fa-link"></i></a>
                                    </div> 
                                </div>
                                <a href="ads-details.html" class="item-name">Feugiat nulla facilisis</a>  
                                <span class="price">$300</span>  
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-item">
                                <div class="carousel-thumb">
                                    <img src="<?php echo get_template_directory_uri() . '/assets/img/product/img8.jpg';?>" alt=""> 
                                    <div class="overlay">
                                        <a href="ads-details.html"><i class="fa fa-link"></i></a>
                                    </div> 
                                </div> 
                                <a href="ads-details.html" class="item-name">Lorem ipsum dolor sit</a>  
                                <span class="price">$149</span> 
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-item">
                                <div class="carousel-thumb">
                                    <img src="<?php echo get_template_directory_uri() . '/assets/img/product/img8.jpg';?>" alt=""> 
                                    <div class="overlay">
                                        <a href="ads-details.html"><i class="fa fa-link"></i></a>
                                    </div> 
                                </div>
                                <a href="ads-details.html" class="item-name">Sed diam nonummy</a>  
                                <span class="price">$90</span> 
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-item">
                                <div class="carousel-thumb">
                                    <img src="<?php echo get_template_directory_uri() . '/assets/img/product/img8.jpg';?>" alt=""> 
                                    <div class="overlay">
                                        <a href="ads-details.html"><i class="fa fa-link"></i></a>
                                    </div> 
                                </div>                     
                                <a href="ads-details.html" class="item-name">Praesent luptatum zzril</a>  
                                <span class="price">$169</span> 
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-item">
                                <div class="carousel-thumb">
                                    <img src="<?php echo get_template_directory_uri() . '/assets/img/product/img8.jpg';?>" alt=""> 
                                    <div class="overlay">
                                        <a href="ads-details.html"><i class="fa fa-link"></i></a>
                                    </div> 
                                </div>  
                                <a href="ads-details.html" class="item-name">Lorem ipsum dolor sit</a>  
                                <span class="price">$79</span> 
                            </div>
                        </div>
                        <div class="item">
                            <div class="product-item">
                                <div class="carousel-thumb">
                                    <img src="<?php echo get_template_directory_uri() . '/assets/img/product/img8.jpg';?>" alt=""> 
                                    <div class="overlay">
                                        <a href="ads-details.html"><i class="fa fa-link"></i></a>
                                    </div> 
                                </div>
                                <a href="ads-details.html" class="item-name">Sed diam nonummy</a>  
                                <span class="price">$149</span>   
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </section>
    <!-- Featured Listings End -->

    <!-- Start Services Section -->
    <div class="features">
        <div class="container">
            <div class="row">
                <?php
                    $benefit = get_post_by_title('home_page_benefit_layout');                    
                    $benefit_id = !empty($benefit)?$benefit->ID:"";
                    $inc = 0;
                    if($benefit_id != "") {
                        for($i=1;$i<=3;$i++) {
                            if(get_field('content_title_'.$i,$benefit_id)) {
                                ?>
                                <div class="col-md-4 col-sm-6">
                                    <div class="features-box wow fadeInDownQuick" data-wow-delay="<?php echo 0.3*$i;?>s">
                                        <div class="features-icon">
                                            <i class="fa fa-book">
                                            </i>
                                        </div>
                                        <div class="features-content">
                                            <h4>
                                                <?php the_field('content_title_'.$i,$benefit_id); ?>
                                            </h4>
                                            <p>
                                                <?php the_field('content_details_'.$i,$benefit_id);?>                                                
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <?php                                
                            }
                        }
                    } else {
                        echo "NO Data Found";
                    }
                ?>                               
            </div>
        </div>
    </div>
    <!-- End Services Section -->

    <!-- Location Section Start -->
    <section class="location">
        <div class="container">
            <div class="row localtion-list">
                <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInLeft animated" data-wow-delay="0.5s" style="visibility: visible;-webkit-animation-delay: 0.5s; -moz-animation-delay: 0.5s; animation-delay: 0.5s;">
                    <h3 class="title-2"><i class="fa fa-envelope"></i> 
                    <?php
                        if(get_field('subcribe_title',$home_id)) {
                            the_field('subcribe_title',$home_id);
                        } else {
                            echo "NO Data Found";
                        }
                    ?></h3>
                    <form id="subscribe" action="">
                        <p><?php
                        if(get_field('subscribe_content',$home_id)) {
                            the_field('subscribe_content',$home_id);
                        } else {
                            echo "NO Data Found";
                        }
                    ?></p>
                        <div class="subscribe">
                            <div class="form-group is-empty"><input class="form-control" name="EMAIL" placeholder="Your email here" required="" type="email"><span class="material-input"></span></div>
                            <button class="btn btn-common" type="submit">Subscribe</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 wow fadeInRight animated" data-wow-delay="1s" style="visibility: visible;-webkit-animation-delay: 1s; -moz-animation-delay: 1s; animation-delay: 1s;">
                    <h3 class="title-2"><i class="fa fa-search"></i> Popular Searches</h3>
                    <ul class="cat-list col-sm-4">
                        <li> <a href="account-saved-search.html">puppies</a></li>
                        <li> <a href="account-saved-search.html">puppies for sale</a></li>
                        <li> <a href="account-saved-search.html">bed</a></li>
                        <li> <a href="account-saved-search.html">household</a></li>
                        <li> <a href="account-saved-search.html">chair</a></li>
                        <li> <a href="account-saved-search.html">materials</a></li>
                    </ul>
                    <ul class="cat-list col-sm-4">
                        <li> <a href="account-saved-search.html">sofa</a></li>
                        <li> <a href="account-saved-search.html">wanted</a></li>
                        <li> <a href="account-saved-search.html">furniture</a></li>
                        <li> <a href="account-saved-search.html">van</a></li>
                        <li> <a href="account-saved-search.html">wardrobe</a></li>
                        <li> <a href="account-saved-search.html">caravan</a></li>
                    </ul>
                    <ul class="cat-list col-sm-4">
                        <li> <a href="account-saved-search.html">for sale</a></li>
                        <li> <a href="account-saved-search.html">free</a></li>
                        <li> <a href="account-saved-search.html">1 bedroom flat</a></li>
                        <li> <a href="account-saved-search.html">photo+video</a></li>
                        <li> <a href="account-saved-search.html">bmw</a></li>
                        <li> <a href="account-saved-search.html">Land </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- Location Section End -->
</div>

<!-- Counter Section Start -->
<section id="counter">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="counting wow fadeInDownQuick" data-wow-delay=".5s">
                    <div class="icon">
                        <span>
                            <i class="lnr lnr-tag"></i>
                        </span>
                    </div>
                    <div class="desc">
                        <h3 class="counter">12090</h3>
                        <p>Regular Ads</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="counting wow fadeInDownQuick" data-wow-delay="1s">
                    <div class="icon">
                        <span>
                            <i class="lnr lnr-map"></i>
                        </span>
                    </div>
                    <div class="desc">
                        <h3 class="counter">350</h3>
                        <p>Locations</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="counting wow fadeInDownQuick" data-wow-delay="1.5s">
                    <div class="icon">
                        <span>
                            <i class="lnr lnr-users"></i>
                        </span>
                    </div>
                    <div class="desc">
                        <h3 class="counter">23453</h3>
                        <p>Reguler Members</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="counting wow fadeInDownQuick" data-wow-delay="2s">
                    <div class="icon">
                        <span>
                            <i class="lnr lnr-license"></i>
                        </span>
                    </div>
                    <div class="desc">
                        <h3 class="counter">150</h3>
                        <p>Premium Ads</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<link rel="stylesheet" href="http://www.amcharts.com/lib/3/ammap.css" type="text/css">
<script src="http://www.amcharts.com/lib/3/ammap.js"></script>
<script src="http://www.amcharts.com/lib/3/maps/js/congoDRHigh.js"></script>
<script type="text/javascript">
var $CHART$ = AmCharts.makeChart("CHART", {
  "type": "map",
  "dataProvider": {
    "map": "congoDRHigh",
    "getAreasFromMap": true
  },
  "areasSettings": {
    "autoZoom": true,
    "selectedColor": "#CC0000",
    "rollOverOutlineColor": "#000000",
    "color": "#5CACE2"
  },
  "smallMap": {},
  "responsive": {
    "enabled": true
  }
});
</script>
<!-- Counter Section End -->
<?php get_footer(); ?>