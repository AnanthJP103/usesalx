<?php
/* Initial theme setup
 * written by : AnanthJP
 * Date : 26-04-2017
 * Modified Date : 26-04-2017
 * ref-link : https://codex.wordpress.org/Plugin_API/Action_Reference/after_setup_theme
 */
function usesalx_initial_theme_setup() {
    // This theme styles the visual editor with editor-style.css to match the theme style.
    add_editor_style();
    // Post Format support. You can also use the legacy "gallery" or "asides" (note the plural) categories.
    add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );
    // Featured image need to display
    add_theme_support( 'post-thumbnails');
    // Add default posts and comments RSS feed links to head
    add_theme_support( 'automatic-feed-links' );
    
    // This theme uses wp_nav_menu() in one location.
//    register_nav_menus( array(
//            'primary' => __( 'Primary Navigation', 'twentytwelve' ),
//            'Secondary' => __( 'Secondary Navigation', 'twentytwelve' ),
//    ) );
}
add_action( 'after_setup_theme', 'usesalx_initial_theme_setup' );

/* Load the style and script file
 * written by : AnanthJP
 * Date : 25-04-2017
 * Modified Date : 26-04-2017
 */
function usesalx_scripts() {
    //Add css style
    wp_enqueue_style( 'bootstrap.min', get_template_directory_uri().'/assets/css/bootstrap.min.css');
    wp_enqueue_style( 'jasny-bootstrap', get_template_directory_uri().'/assets/css/jasny-bootstrap.min.css');
    wp_enqueue_style( 'material-kit', get_template_directory_uri().'/assets/css/material-kit.css');
    wp_enqueue_style( 'font-awesome.min', get_template_directory_uri().'/assets/css/font-awesome.min.css');
    wp_enqueue_style( 'line-icons', get_template_directory_uri().'/assets/fonts/line-icons/line-icons.css');
    wp_enqueue_style( 'main', get_template_directory_uri().'/assets/css/main.css');
    wp_enqueue_style( 'animate', get_template_directory_uri().'/assets/extras/animate.css');
    wp_enqueue_style( 'owl.carousel', get_template_directory_uri().'/assets/extras/owl.carousel.css');
    wp_enqueue_style( 'owl.theme', get_template_directory_uri().'/assets/extras/owl.theme.css');
    wp_enqueue_style( 'responsive', get_template_directory_uri().'/assets/css/responsive.css');
    wp_enqueue_style( 'slicknav', get_template_directory_uri().'/assets/css/slicknav.css');
    wp_enqueue_style( 'bootstrap-select.min', get_template_directory_uri().'/assets/css/bootstrap-select.min.css');
//    wp_enqueue_style( 'ammap', get_template_directory_uri().'/assets/css/ammap.css');
    wp_enqueue_style( 'custom_style', get_template_directory_uri().'/style.css');
    
    //Add js script
    wp_enqueue_script( 'jquery-min', get_template_directory_uri() . '/assets/js/jquery-min.js', array(), false, true );
    wp_enqueue_script( 'bootstrap.min', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), false, true );
    wp_enqueue_script( 'material.min', get_template_directory_uri() . '/assets/js/material.min.js', array(), false, true );
    wp_enqueue_script( 'material-kit', get_template_directory_uri() . '/assets/js/material-kit.js', array(), false, true );
    wp_enqueue_script( 'jquery.parallax', get_template_directory_uri() . '/assets/js/jquery.parallax.js', array(), false, true );
    wp_enqueue_script( 'owl.carousel.min', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array(), false, true );
    wp_enqueue_script( 'wow', get_template_directory_uri() . '/assets/js/wow.js', array(), false, true );
    wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', array(), false, true );
    wp_enqueue_script( 'jquery.counterup.min', get_template_directory_uri() . '/assets/js/jquery.counterup.min.js', array(), false, true );
    wp_enqueue_script( 'waypoints.min', get_template_directory_uri() . '/assets/js/waypoints.min.js', array(), false, true );
    wp_enqueue_script( 'jasny-bootstrap.min', get_template_directory_uri() . '/assets/js/jasny-bootstrap.min.js', array(), false, true );
    wp_enqueue_script( 'bootstrap-select.min', get_template_directory_uri() . '/assets/js/bootstrap-select.min.js', array(), false, true );
//    wp_enqueue_script( 'ammap', get_template_directory_uri() . '/assets/js/ammap.js', array(), false, true );
//    wp_enqueue_script( 'cameroonHigh', get_template_directory_uri() . '/assets/js/cameroonHigh.js', array(), false, true );
    wp_enqueue_script('jquery');
}
add_action( 'wp_enqueue_scripts', 'usesalx_scripts' );
/*
 * Get post by title
 * Written by : AnanthJP
 * Date : 26-04-2017
 * Modified Date : 26-04-2017
 * ref link : https://wordpress.stackexchange.com/questions/11292/how-do-i-get-a-post-page-or-cpt-id-from-a-title-or-slug
 */
function get_post_by_title($page_title, $post_type ='post' , $output = OBJECT) {
    global $wpdb;
        $post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type= %s", $page_title, $post_type));
        if ( $post ) {
            return get_post($post, $output);
        }
    return null;
}
/*
 * Resolved redirect functionality not working on the login page
 * Written by : AnanthJP
 * Date : 26-04-2017
 * Modified Date : 26-04-2017
 * Ref link : http://stackoverflow.com/questions/19587154/wp-redirect-is-not-working
 */
function app_output_buffer() {
    ob_start();
}
add_action('init', 'app_output_buffer');
/*
 * Resolved redirect functionality not working on the login page
 * Written by : AnanthJP
 * Date : 26-04-2017
 * Modified Date : 26-04-2017
 * Ref link : https://haveposts.com/blog/2013/01/25/watch-movie-online-silence-2016-subtitle-english/
 */
function login_link_url( $url ) {
   $url = get_bloginfo( 'url' ) . "/login";
   return $url;
}
add_filter( 'login_url', 'login_link_url', 10, 2 );
