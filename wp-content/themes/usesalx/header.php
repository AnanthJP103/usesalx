<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">    
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="author" content="Clasified">
        <meta name="generator" content="Wordpress! - Open Source Content Management">
        <title>want2announce</title>
        <!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri() . '/assets/img/usesalx_fav.ico';?>">
        <?php wp_head(); ?>
    </head>

    <body>  
        <!-- Header Section Start -->
        <div class="header">    
            <nav class="navbar navbar-default main-navigation" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <!-- Stat Toggle Nav Link For Mobiles -->
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- End Toggle Nav Link For Mobiles -->
                        <a class="navbar-brand logo" href="index.html"><img src="#" alt=""></a>
                    </div>
                    <!-- brand and toggle menu for mobile End -->

                    <!-- Navbar Start -->
                    <div class="collapse navbar-collapse" id="navbar">
                        <ul class="nav navbar-nav navbar-right">
                            <?php
                            if (is_user_logged_in()) {
                                ?><!--https://linearicons.com/free-->
                                <li><a href="<?php echo get_permalink(get_page_by_path('login')); ?>"><i class="lnr lnr-user"></i> profile</a></li>
                                <li><a href="<?php echo wp_logout_url(home_url()); ?>"><i class="lnr lnr-lock"></i> Logout</a></li>
                                <?php
                            } else {
                                ?>
                                <li><a href="<?php echo get_permalink(get_page_by_path('login')); ?>"><i class="lnr lnr-enter"></i> Login</a></li>
                                <li><a href="<?php echo get_permalink(get_page_by_title('join')); ?>"><i class="lnr lnr-user"></i> Signup</a></li>
                            <?php } ?>
                            <li class="postadd">
                                <a class="btn btn-danger btn-post" href="post-ads.html"><span class="fa fa-plus-circle"></span> Post an Ad</a>
                            </li>
                        </ul>
                    </div>
                    <!-- Navbar End -->
                </div>
            </nav>
            <!-- Off Canvas Navigation -->
            <div class="navmenu navmenu-default navmenu-fixed-left offcanvas"> 
                <!--- Off Canvas Side Menu -->
                <div class="close" data-toggle="offcanvas" data-target=".navmenu">
                    <i class="fa fa-close"></i>
                </div>
                <h3 class="title-menu">All Pages</h3>
                <ul class="nav navmenu-nav"> <!--- Menu -->
                    <li><a href="index.html">Home</a></li>
                    <li><a href="index-v-2.html">Home Page V2</a></li>
                    <li><a href="about.html">About us</a></li>            
                    <li><a href="category.html">Category</a></li>             
                    <li><a href="ads-details.html">Ads details</a></li>    
                    <li><a href="pricing.html">Pricing Tables</a></li>    
                    <li><a href="account-archived-ads.html">Account archived</a></li>
                    <li><a href="account-close.html">Account-close</a></li>
                    <li><a href="account-favourite-ads.html">Favourite ads</a></li>
                    <li><a href="account-home.html">Account home</a></li>
                    <li><a href="account-myads.html">Account myads</a></li>
                    <li><a href="account-pending-approval-ads.html">pending approval</a></li>
                    <li><a href="account-saved-search.html">saved search</a></li> 
                    <li><a href="post-ads.html">Post ads</a></li> 
                    <li><a href="posting-success.html">Posting-success</a></li>  
                    <li><a href="blog.html">Blogs</a></li>
                    <li><a href="blog-details.html">Blog Details</a></li>
                    <li><a href="contact.html">Contact</a></li>
                    <li><a href="forgot-password.html">Forgot-password</a></li>
                    <li><a href="faq.html">Faq</a></li>
                    <li><a href="signup.html">Signup</a></li>
                </ul><!--- End Menu -->
            </div> <!--- End Off Canvas Side Menu -->
            <div class="tbtn wow pulse" id="menu" data-wow-iteration="infinite" data-wow-duration="500ms" data-toggle="offcanvas" data-target=".navmenu">
                <p><i class="fa fa-file-text-o"></i> Menu</p>
            </div>
        </div>
        <!-- Header Section End -->